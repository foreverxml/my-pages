---
title: "Make a Server for You and your Friends"
date: 2022-05-05T13:06:26Z
draft: false
---

### *This article is very much work-in-progress!*

Making a server for you and your friends is very easy if you have some tech knowledge. I’ll outline it below.

## Time to get a Domain

There are many, many domain providers. For this guide I’ll be using <a class="h-entry p-name u-url" href="https://namecheap.com">Namecheap</a>. First, you click that link (the word *Namecheap*) and visit the site. There’s a big search bar in the middle: start typing in your domain there. You can do anything here. You could do your name (`johnappleseed`), an online username (`parzival`), or anything. Then, click the *Search* button.<!--more-->

Lots of results will come up, and there will be 5 highlighted. You can add one of those to the card, or scroll and find more. You can also type a specific domain ending (`.cc`, `.co.uk`, `.online`) into the search bar after your site name and it will highlight that. Keep searching until you find a domain, then checkout. This guide will assume you bought the domain `parzival.dev`.

## Time to move DNS

Using DigitalOcean for your DNS makes things simpler when you have a domain. DNS is what makes it so that websites load on your computer/phone.

First, go to <a class="h-entry p-name u-url" href="https://digitalocean.com">DigitalOcean</a> and make an account, and complete setup. You should have a dashboard. In this dashboard, there’s a sidebar; in that sidebar, click *Networking* and then in the page that loads click *Domains*. You will have a project selected and a text box to type your domain. Type the domain and click "Add". Then, in Namecheap, go to <a class="h-entry p-name u-url" href="https://ap.www.namecheap.com/">the dashboard</a> then click *Manage* next to your domain. In the dropdown next to *Nameservers*, select *Custom DNS* and add these domains:
```
ns1.digitalocean.com
ns2.digitalocean.com
ns3.digitalocean.com
```
Then save it. Now, in DigitalOcean, click the domain name to go to its dashboard: click the `NS` tab. Now, add the following:
```
Hostname | Will direct to
@        | ns1.digitalocean.com
@        | ns2.digitalocean.com
@        | ns3.digitalocean.com
```
Don’t touch if it they already exist. And, you have moved the control to DigitalOcean, which is referenced later in this guide. *Keep this tab open, or know how to access it!*

## Time to get the Droplet

In DigitalOcean again, there’s a top bar. Click *Create* and in the dropdown *Droplets*. Leave the options be, except:

- In *CPU Options*, click *Basic* and then the cheapest option.
- Choose a *Datacenter Region* you like.
- Add a *Password* in.
  - **DO** ***NOT*** forget this password! You will lose droplet access.
- Enable *IPv6* and *Monitoring* in *Additional Options*
- Choose a "hostname" for your Droplet

Then click create, and it should be made for you. Now, in the sidebar, click *Billing* and add in your payment info. Congrats, you have a DigitalOcean Droplet ready to run software!

## Time to install Requirements

Click on the Droplet name in your Project, which is what loads by default in the DigitalOcean dashboard. Then, click *Console* and a new web tab should launch, putting you in a terminal. In this terminal, copy and paste (with `Ctrl + Shift + V`) the following:
```sh
apt update
apt upgrade
apt install caddy cargo postgresql docker docker-compose -y
systemctl stop caddy.service
```
You should now have the Requirements installed to run a server with Libreddit, WriteFreely, and Bin.

## Time to get WriteFreely

WriteFreely is a great blogging service. If you are reading this, chances are it’s on WriteFreely. It is very simple to install. According to the <a class="h-entry p-name u-url" href="https://writefreely.org/start">WriteFreely installation directions</a>, it is simple. Here’s what I reccomend.

### Time to Setup

Click <a class="h-entry p-name u-url" href="https://github.com/writefreely/writefreely/releases/latest">here</a>, and copy the link to the one that says `writefreely_{version number}_linux_amd64.tar.gz`. Then, in that command line:
```sh
curl -Lo writefreely.tar.gz {link here} && tar -xf writefreely.tar.gz && cd writefreely && ./writefreely config start
```
For **Environment**, choose **Production, behind reverse proxy**. Then, go through the setup and choose a subdomain for your WriteFreely to live on. *This can’t be changed*. Examples can be, with domain `parzival.dev`:
```
blog.parzival.dev
wf.parzival.dev
write.parzival.dev
```
Any, as long as they are not the top-level domain `parzival.dev` for example. Unless you’re feeling confident you won’t put anything else there. Continue selecting values you like, except:

- Set *Port* to 499.
- Choose *Multi-user instance* when prompted.
- Use SQLite for the database and the default `writefreely.db` file.
- Disable *Open signups*.

You should be good now! Paste in:
```sh
./writefreely keys generate
./writefreely &
cd ..
touch Caddyfile
```
WriteFreely is running now, yay! Now, it’s time to allow the world to see it. You now want to paste in:
```sh
nano Caddyfile
```
And a text editor will launch. Copy and paste this in:
```plain
blog.parzival.dev {
    encode zstd gzip
    reverse_proxy localhost:499
}
```
Change `blog.parzival.dev` to your domain you put in the configuration earlier. Now, go to the DigitalOcean DNS control panel, and go to the `A` tab. In it, put in the subdomain (`blog` of `blog.parzival.dev`) in the name field, in the IP field select your droplet, and then click *Create*. Repeat in the `AAAA` tab. Now, return to the console, and type:
```sh
caddy run &
```
Now, visit your subdomain. It should have WriteFreely on it! There’s one last thing to do though- create the admin. It’s easy, just do this:
```sh
cd writefreely && ./writefreely --create-admin {username}:{password} && cd ..
```
Replace {username} and {password} with your username and password. Then, your WriteFreely should be running and configurable!

## Time to get Libreddit

Libreddit allows you to see things from Reddit easily. First, run this command:
```sh
cargo install libreddit
```
This command should take a long time. Then, run
```sh
export {VARNAME)={value}
```
where `{VARNAME}` is the variable name and `{value}` is the value, both from [here](https://github.com/spikecodes/libreddit#change-default-settings). Finally, run
```sh
export PATH=/root/.cargo/bin:$PATH
libreddit &
```
Libreddit is now running. Again, run `nano Caddyfile` and put at the end:
```plain
libreddit.parzival.dev {
    encode zstd gzip
    reverse_proxy localhost:8080
}
```
Replace `libreddit.parzival.dev` with a subdomain of your domain as explained earlier that you want it to be under. You can change this one later. Now, go to the DigitalOcean DNS control panel and do the steps in the WriteFreely guide, except with your libreddit subdomain. Finally, run
```sh
caddy reload
```
and your Libreddit should be running!

## Time to get Bin

Bin is a simple pastebin that you can use to host your content. It is very easy to install. To install it, run these commands:
```sh
touch docker-compose.yml
nano docker-compose.yml
```
Then, copy this in:
```yaml
version: '3.3'
services:
  pastebin:
    image: wantguns/bin
    container_name: pastebin
    ports:
      - 127.0.0.1:6163:6163
    environment:
      - BIN_PORT=6163
      - BIN_LIMITS={form="10 MiB"}
      - BIN_CLIENT_DESC="Bin"
    volumes:
      - ./upload:/upload
```
You may want to change `10 MiB` to be larger in size. Then, when you’re done, press Ctrl+X, Y, and Enter. Finally, run:
```sh
docker-compose up -d
nano Caddyfile
```
and add
```plain
bin.parzival.dev {
    encode zstd gzip
    reverse_proxy localhost:6163
}
```
to it, replacing `paste.parzival.dev` with a subdomain of your choice (that can be changed later), and exit with Ctrl+X, Y, and Enter. Now, navigate into your DigitalOcean DNS console and do the steps in the WriteFreely guide, except with your bin subdomain. Finally, navigate back to your console and type
```sh
caddy reload
```
and you should have a shiny new pastebin!

### How to limit Pasting?

To limit it you need to put authentication on it. This is easy. Simply run
```sh
caddy hash-password --plaintext "good password here"
```
replacing `good password here` with your preferred password, and copy the output. Then, run `nano Caddyfile` again and replace your bin configuration with:
```plain
bin.parzival.dev {
    encode zstd gzip
    basicauth / {
        user password
    }
    reverse_proxy localhost:6163
}
```
replacing your subdomain in, a preferred user for `user`, and the copied output of the hash-password for `password`. Then, again, run
```sh
caddy reload
```
and it will now be password-protected to upload pastes.
