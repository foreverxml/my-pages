---
title: "Caddy Starting Guide"
date: 2022-05-20T21:09:57Z
draft: false
---

So, you want to get started with making a website. You’ve got a shiny new cloud server, and your blog and website figured out, but what from here? Well…

## Install Caddy

The <a class="h-entry p-name u-url" href="https://caddyserver.com">Caddy server</a> makes websites a cinch. All you need is a domain, Caddy, simple command line knowledge, and something to serve. So, let’s get started! First, install it. Make sure you have an SSH session into your server, then run the commands:
```sh
# On Debian/Ubuntu
sudo apt install caddy -y
# On Fedora
sudo dnf install caddy -y
# On all 3
sudo systemctl stop caddy
```

This will install Caddy on your server, and allow it to be configured. Hooray!

## Basic Caddy Commands

Most of the commands you’ll need to know are the following:
```sh
# starts Caddy with the Caddyfile in the current directory
caddy start
# stops Caddy
caddy stop
# applies new config
caddy reload
# formats the Caddyfile
caddy fmt --overwrite
# makes a password
caddy hash-password --plaintext "AGoodPassword"
```
These are the primary ones we’ll use.

## Serve some files!
To serve some files, go to your `/root` directory and make a file called `Caddyfile` with the contents:
```plain
caddy.aroace.space {
	encode zstd gzip
	root * /root/webthing
	file_server
}
```
Then, replace `caddy.aroace.space` with your domain, make sure you have A and AAAA records pointed at your server from that domain, then run `caddy start`. You now have a simple static site serving from `/root/webthing` running, with HTTPS and security!
### Line-by-line analysis
#### `caddy.aroace.space {`
This line defines what config to use for this domain.
#### `encode zstd gzip`
Compresses responses with Zstandard, and if that’s not supported then GZip.
#### `root * /root/webthing`
Sets where to base the file server off.
#### `file_server`
Like it says.
## Make it faster!
Modify your `Caddyfile` as below:
```diff
+ {
+	servers {
+		protocol {
+			experimental_http3
+		}
+	}
+ }
caddy.aroace.space {
	encode zstd gzip
+	header {
+		Cache-Control "max-age=604800, stale-if-error=2419200"
+		Strict-Transport-Security max-age=31536000
+	}
+	header *.html Cache-Control "max-age=600, stale-if-error=2419200"
+	header *.css Cache-Control "max-age=600, stale-if-error=2419200"
	root * /root/webthing
	file_server
}
```
this will allow your website to be more secure, and not waste users’ bandwidth. in a nutshell:

- enable a new quicker connection that drops less
- cache files so that not as many connections are made
- cache html and css for less long
- make the website more secure

yea, that’s mostly it for now. check this page for updates soon! :)