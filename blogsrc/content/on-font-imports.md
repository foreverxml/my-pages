---
title: "On Font Imports"
date: 2022-05-01T13:04:06Z
draft: false
---

I’ve seen a lot of people discussing why font importing is *bad*. I don’t think that’s true, for a variety of reasons. Here are my reasons.

## Fonts are tiny

Inter, with only regular style and Latin and symbols, is an 16KB WOFF2 file. You also need only 5 lines of CSS to make a font available.

```css
@font-face {
    font-family: Inter;
    src: local("Inter"), url("./inter.woff2") format("woff2");
    font-display: swap;
}
```

It’s that easy. No extra stuff. And once again, *it’s 16KB*. And you’re wondering, *But that will take **forever** to load!* I give you, the next point.

## `font-display: swap` saves us all

In the CSS above, there’s a line that is `font-display: swap;`, right? This line ensures the page is loaded *before* the font, and when the font is loaded it will swap it in. So, even if it’s too large, *the font loads AFTER the page* so your page’s still snappy.

## But I want my custom font to apply to all pages!

If you’ve invested the tech into getting a custom setup with a custom font and everything, you should also be able to block font files from loading. In fact, there’s even a [Font Blocker](https://chrome.google.com/webstore/detail/font-blocker/knpgaobajhnhgkhhoopjepghknapnikl) Chrome extension. It’s so small, it could be reimplemented on basically any browser.

## But… why can’t people just *not* import fonts?

Sadly, the defaults in basically *all* browsers **really suck**. And most people either:

- Don’t know how to change the defaults
- Can’t change the defaults
- Can’t install the font that your site looks best in

Those people include me. Whenever I go to a site without an imported font, it defaults to Arial (which, for the life of me, I can’t read). And on sites without even a `font-family` (yes, they exist), it becomes another unreadable font, but this time it’s serif.

## Phones…

Many people use *phones* for browsing the web. Including me. You *can’t* choose fonts on phones, mainly because you can’t install them. And they’re stuck with the defaults… so… consider importing fonts to make your phone experience better, hm?

## That’s all for now.

If I find any more points I think fit here, I’ll add them in. Peace!
