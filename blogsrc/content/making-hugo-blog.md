---
title: "Making a Hugo Blog"
date: 2022-05-19T18:18:36Z
draft: false
---

It’s not hard starting to make a blog with <a class="h-entry p-name u-url" href="https://gohugo.io">Hugo</a>.  All it requires is knowing how to use Markdown and Git, and if you want to make your own theme, knowing how to use HTML with Go templating. Anyway, I’ll outline how to make a blog with Hugo now.
## Get Set Up
First, <a class="h-entry p-name u-url" href="https://gohugo.io/getting-started/installing">install Hugo</a> and <a class="h-entry p-name u-url" href="https://git-scm.com/">git</a>. Then, once that’s done, run:
```sh
hugo new site pages
cd pages
git init
```
This should make a new **Hugo site** with a **Git repository** which will allow easy website deployment and syncronization.

You should be aware that this guide assumes that you are using a Mac/Linux computer
### I want to deploy my site on Github/Codeberg Pages
#### On GitHub
Make a repository underneath your username, one named `my-pages` and one named `{username}.github.io` where `{username}` is your username. Make sure both are empty, and you have <a class="h-entry p-name u-url" href="https://docs.github.com/en/authentication/connecting-to-github-with-ssh">SSH in GitHub</a>.
#### On Codeberg
Make a repository `my-pages` and `pages`, both need to be empty. Make sure you have <a class="h-entry p-name u-url" href="https://docs.codeberg.org/security/ssh-key/">SSH in Codeberg</a>.
#### For Both
Run the following commands in your terminal (the terminal is very often used here):
```sh
cd public
git submodule add {subLink}
```
Replace `{subLink}` with the following URL, replacing `{username}` with your username:
- GitHub: `git@github.com:{username}/{username}.github.io.git`
- Codeberg: `git@codeberg.org:{username}/pages.git`

This should make it so that when Hugo makes your site, it can be deployed on your page directly.
## <a class="h-entry p-name u-url" href="https://gohugo.io/getting-started/quick-start/#step-3-add-a-theme">Follow the Hugo tutorial…</a>
The Hugo tutorial is a good starting point for making a good blog.
### Deploying with Git
To deploy once you build with `hugo`, run the following commands:
```sh
cd public
git add -A
git commit -m "Update page content"
git push
```
and it should update the static page.
### Deploying with Caddy
Once you run `hugo`, move out of the `pages` directory to the directory containing it and run these commands:
```sh
scp -r pages/ root@{your server ip/addr}:/root/pages
ssh root@{your server ip/addr}
# in ssh
nano Caddyfile
# in nano, add this for your site
"""
blog.example.com {
    root * /root/pages/public
    file_server
}
"""
# replacing the url with the one you have, then save and exit and run
caddy reload
```
This assumes you are running the <a class="h-entry p-name u-url" href="https://caddyserver.com">Caddy webserver</a> on your server.
## …or make your own theme
In the sites directory, run the command:
```sh
hugo theme new mytheme
```
and add
```toml
theme = mytheme
```
to your `config.toml`. You can modify the files following:
```sh
themes/mytheme/layouts/partials/head.html # head in HTML for every site
themes/mytheme/layouts/partials/header.html # header for every site
themes/mytheme/layouts/partials/footer.html # footer for every site
themes/mytheme/layouts/_default/baseof.html # base of every site
themes/mytheme/layouts/_default/single.html # base of viewing 1 blog post
themes/mytheme/layouts/_default/list.html # list of posts
themes/mytheme/layouts/index.html # homepage
```
You may also run the following command to get a more sensible RSS template:
```sh
cd themes/mytheme/layouts/_default
curl -Lo index.rss.xml https://codeberg.org/foreverxml/my-pages/raw/branch/main/blogsrc/themes/aroacespace/layouts/_default/index.rss.xml
```
This requires `curl` to be installed.
