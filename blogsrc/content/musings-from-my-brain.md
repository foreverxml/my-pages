---
title: "Musings from my brain"
date: 2022-05-04T13:02:14Z
draft: false
---

*As the snow falls soft
Lit by only the bright moon,
I feel pure calmness*

This week’s been a rough ride. I lost some friends, the aggression has been happening, and more. But that aside. I will rant about other things. They may be hot topics, especially if you’re a straight man.

#### *This post uses reclaimed slurs and curse words.*

## The Behavior of Straight Men is, in a nutshell, Trash

*Phew*, this is gonna be a hot one. So, ahem. I’m still in grade school (not revealing my exact grade, somewhere about or near high school). And misogynism is *already* forming in the boys in my school. Get ready for *drama*, and this document *does have* slurs that I’m permitted to say.

### Example One

Local idiot, who gives out N-word passes and his friends say racial slurs with him, says he "hates women and men are better". He then, proceeds to look for a girlfriend.

Calling women “faggot” (Yes, he called me that. I’m a lesbian.) isn’t gonna get you a girlfriend. It’s not funny.

### Example Two

Sits next to me in art and science. Made a list of everyone. With gender and pronouns. Creepy as hell. Furthermore, a classmate (she/they) went up to him and said “I’m she/they, could you please change that?”

He said, and I quote:

> No, because female is your biological gender.

What the hell are you doing? Are you making a who’s hot and who’s not list? Stop fucking objectifying people based on assumed birth anatomy. I could be trans and you wouldn’t even know. Shut the fuck up.

### Example Three

There’s a kid in my school. He says retard too much. He’s NT. He’s called me retard before. If the Dojo Kun in karate did not say “refrain from violent behavior”, I would have punched his face.

I also think [this post](https://write.disqordia.space/changeling/thinking-about-autism-and-humanity) by Alice Disqordia is relevant.

### Example Four

This one’s so bad, I’m giving you a name, an unaliased name: Logan.

I’m not using a made up name. This is his *actual* name. Lucky for you, scheißkopf, there are enough Logans out there so that you don’t get exposed. Oh, if you’re a friend of him: You too.

These assholes. Every *fucking* time I came into math, they commented on how "hot" I was. That’s #1 on the reasons why. I’m aro ace, but also lesbian, and I assert that aggressively. On to #2. They made these same "compliments" to my friend who wears hijab. Not wearing a hijab, she wears hijab. It means dressing modestly. I do it too, but without the hijab (now I refer to the headscarf). *Very* inappropriate to do that.

Number 3, they wanted to go out with me. They’ve asked me out on a date, I think, 15 times already. In total. I respond No in a different language each time, with a different insult each time.

Number 4, it doesn’t stop here. They also are in my first period, asking for my Discord because I use element. You shitheads, your dream gets more impossible with each reason.

Number 5, what a surprise. When you bullied me to the tipping point, with all of this, and I said “I don’t trust men” (see previous examples for adding to this evidence).

And you laughed, you *fucking laughed*. My tablemate asked why? You said “Because Forever hurt my feelings”.

That, especially that. If I had to choose one reason to slap all of you, it’s that. Forget the rest.

How dare you be this way and want a girlfriend. And then say women are the bad ones? Please.

## Oh… but it’s not *just* straight men.

### Example 5: Lesbian-Straight Women

As in Example 4, they harassed me asking for a “date”. I’m not fucking interested.

### Example 6: Straight Women

Helping the team from Example 4 make me suicidal. Thank goodness it didn’t get that far.

## There’s still more…

Thanks for staying around.

These aren’t my only problems in this school. That’s just the beginning. I can list more if you want. You do? Okay.

- I got called a retard for speaking German.
- When voicing my story with Example 1 I got called a racist, and even though the person in it is Black, this has nothing to do with race.
- I *normally* get asked for my Discord when I have Element open.
- People abandon me in “teamwork” making my grade shit when I work so hard.

And some more for Example 4:

- Said “imagine dying” to someone who’s Grandmother died of covid.
- Assosciate with Example 1, who gives the "n-word pass" and they say it to one another.

That’s about it. That’s my rant. Until next time.
