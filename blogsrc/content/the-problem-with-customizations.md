---
title: "The Problem With Customizations"
date: 2022-05-02T13:03:47Z
draft: false
---

*Open computer
Take in the pretty themes, too
Until it all breaks*

Don’t get me wrong. Customizing is really nice. In fact, I do make a couple of GNOME purple themes. But customization, as a concept, has fatal flaws in computing. This, in some cases, extends to preferences too.

*This work contains my opinions on theming, preferences, and more.*<!--more-->

## Let’s Start With App Icons

Ready? Alright. I make my app, {{< url href="https://forever.amongtech.cc/random" title="Random" >}}, for GNOME. I have everything in. Then, someone makes an icon for Numix Circle (that icon’s {{< url href="https://raw.githubusercontent.com/numixproject/numix-icon-theme-circle/master/Numix-Circle/48/apps/randomgtk.svg" title="here" >}}). And then, my Random icon is updated. (That one’s {{< url href="https://codeberg.org/foreverxml/random/raw/branch/main/data/icons/hicolor/scalable/apps/page.codeberg.foreverxml.Random.svg" title="here" >}}). Let’s quickly compare these two. (Please open both and look at them).

You’ve looked at both, right? Good. The Numix Circle one is modeled after the old Random icon, which was modeled after the GNOME Clock icon. Now, there’s a new, *better* icon, but the custom icon isn’t updated. This reveals #1 on our list of the flaws with customization:

- 1. When customizations are made, they’re not usually updated with the app.

Also, app icons can look out of place with all of them being in one theme, and one native app *not* being in that one theme. That’s #2:


- 2. Not-customized apps will look out of place.
- 3. Furthermore, many users will uninstall a not-customized app *because* it looks out of place.

That’s already 3 flaws, and that’s *just* looking at app icons. So, without further ado, let’s look at other things.

## App “Themes”

I’ll parse {{< url href="https://blogs.gnome.org/tbernard/2018/10/15/restyling-apps-at-scale/" title="this article" >}} by Tobias Bernard. First, however, let’s look at some broken “themes”.

{{< figure src="https://blogs.gnome.org/tbernard/files/2018/08/pop-calendar.png" alt="Pop!_OS date picker" caption="Date picker in Calendar on Pop OS: There are double arrows left of the month and year labels." >}}

{{< figure src="https://blogs.gnome.org/tbernard/files/2018/08/ambiance-createfile.png" alt="Nautilus in Ambiance" caption="“New Folder” dialog in Nautilus with Ambiance: There’s an invisible “Create” button in the top right." >}}

{{< figure caption="Gedit on Pop!_OS: There’s a brown rectangle sticking out of the window at the top, and the widgets at the top of the sidebar look awkward and don’t make sense in this configuration." alt="Gedit on Pop!_OS" src="https://blogs.gnome.org/tbernard/files/2018/10/gedit-sidebar-pop.png" >}}

These are just a couple examples of what happen when an app is themed, and then something is updated. Back to number 1, I guess.

And, also reference back to 2 and 3. Wow, those too! One other thing, though:

- 4. Help docs can’t work if the app looks different.
- 5. It complicates things for the developer, and makes the app hard to maintain.

Now that’s it for here.

## What happens if these are *default*?

This adds some more.

- 6. It removes the possiblity to create a cohesive identity.

Not much more, but there’s that. There’s things that people who enforce sometimes do (here, I look at Pop!_OS’s reaction to Libadwaita). So, in summary:

## The 6 Things Wrong With Customization

1. When customizations are made, they’re not usually updated with the app.
2. Not-customized apps will look out of place.
3. Furthermore, many users will uninstall a not-customized app *because* it looks out of place.
4. Help docs can’t work if the app looks different.
5. It complicates things for the developer, and makes the app hard to maintain.
6. It removes the possiblity to create a cohesive identity.

That’s it to my article on this. Please know I’m not against this, and don’t have hard feelings for people who like customization. I sometimes like customization. I’m just pointing out some base flaws. Thanks for hanging around.
