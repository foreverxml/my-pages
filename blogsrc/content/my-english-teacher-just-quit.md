---
title: "My English Teacher Just Quit"
date: 2022-05-02T15:02:39Z
draft: false
---

A week ago, the mask mandate was lifted in my school. Every teacher has a first period; including my English teacher. People said, that in the middle of that class she just left the class without her stuff. Nobody’s seen her since.

Today, I got the news she quit. I’m very mad about this, as she didn’t even respond to *text messages to her* from the admins.

## A Timeline Of Days

- It’s Feburary 28, 2022. The day she left in first period. I file into ELA class, and there’s a sub. I think nothing of it. *Maybe she’s just sick*.
- March 1. Another day, another sub. No worries, *still maybe sick*.
- March 2. Another sub? I get told that she isn’t responding to texts. *Is everything okay?* I fail my class assignment, because this sub told us nothing.
- March 3. Still another sub. I fail class assignment again. These are the first two fails I’ve had in English ever. *What’s happening?*
- March 4, Friday. Same sub from March 2. Not too much work. *I don’t think everything’s okay…*
- Today, March 7. I get told that she quit. *Something’s definetely not okay.*

## Why Am I So Mad?

My education quality will be decreased, for the remainder of the time I have in this grade. This *could* extend into college, where I might not get as good grades, and then never be able to leave the USA (where everything is expensive and I will *most probably* fall into poverty.)

While writing this, I’m also having period cramps. The cherry on top. Well, some sort of cramps. I don’t know what type.

It’s pretty miserable to have a class that makes you angry, where *nothing* is done,*and* have cramps.

Not the best day, in my opinion.

## On The Other Hand, I’m Worried

What could’ve happened there that caused this so sudden leaving? What would have happened?

Maybe the death of a relative, or something else.

Either way, it’s expected you stay there for *at least* one year. If you’re aiming to be a teacher: Don’t quit in the middle of the year

This feels unreal writing this. But it happened.

Until next time.
