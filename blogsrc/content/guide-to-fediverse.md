---
title: "A Fediverse Intro"
date: 2022-07-17T15:12:02Z
draft: false
---
Hello, weary traveler! You have happened upon my guide to the Fediverse, containing all I think is nessescary for new people. Let's begin!
## What even *is* the Fediverse?
Imagine this scenario: A group of people are dissatisfied with Twitter's rules, so they make their own Twitter that people on Twitter can interact with and vice versa. These people on the new Twitter also can interact with Instagram and Facebook and YouTube, and best of all: there are many of this new Twitter. More people make new YouTubes, Instagrams, and Facebooks, and they all talk together.

Now stop imagining it! This is what the Fediverse (referred to as Fedi sometimes) does, except it doesn't talk to Twitter, Facebook, et al. Instead, it can talk to any server implementing the **ActivityPub** protocol, which is what makes the Fediverse. It is not a software, it is a communication method. It can be deployed on a server, and then that server is called an **instance** that engages in **federation** with other instances (basically, communicating).
### What can I use in the Fediverse that is similar to what I already use?
- *Twitter* is now *[Mastodon](https://joinmastodon.org), [GoToSocial](https://gts.superseriousbusiness.org), [Misskey](https://join.misskey.page), and [Akkoma](https://akkoma.dev)*. There's lots of choice! Of course, you may also [mirror from Twitter to Fedi](https://beta.birdsite.live) or [view it privately](https://nitter.42l.fr).
- *Instagram* is now *[Pixelfed](https://pixelfed.org)*.
- *YouTube* is now *[PeerTube](https://joinpeertube.org/)*. You may also view YouTube [privately](https://piped.mint.lgbt).
- *Reddit* has no replacement, but can be viewed [privately](https://aaa.aroace.space).
## There's upsides and downsides, right?
The Fediverse has upsides and downsides, like everything.
### Upsides
- **No tracking or ads** - Ever. The software is simply not designed to do that.
- **No algorithms** - same as above!
- **Make your own rules** - If you don't like someone else's rules, you can make your own community instance. With stricter or more relaxed rules.
- **Domain blocking** - Blocking bad people is more effecient with domain blocking.
- **Custom emojis** (sometimes) - you can make your own emojis to use!
- **Accessibility** - more accessible due to image descriptions.
- **Decentralized** (not like web3 garbage) - no entity can truly own the Fediverse.
- **Content warnings** - hide content behind a CW to warn people.
- **Smaller communities** - allows a sense of togetherness and good moderation.
- **No evil design** - no dark patterns and stuff.

### Downsides

- **Make your own rules** - This is also a downside. Pedophiles, transphobes, bigots, fascists, and nazis can make their own instances. And they do! Thankfully, you can block them easily with domain blocking.
- **Power to the admins** - the server admin has power over all community members. But less so than Twitter.
- **No federation to Twitter and things** - they do not use ActivityPub. They trap you with the *network effect*.

## But all these places are empty, how do I even join?

You need to feel the groove of Fedi. Feel the ups and downs, find people that you can bond with, and surround yourself with nice people. And without any corporate meddling too, Fedi is very anarchist and anti-capitalist.
## I want to join, I will make an account on [`mastodon.social`](https://mastodon.social)!
Wait- no! Please don't. `mastodon.social` is the flagship instance, and all the newcomers join it, overcrowding it. It's too big and can't be moderated effectively, and has ethical concerns, so some users add it to their domain blocks. Same with [`mastodon.online`](https://mastodon.online).
## Waittt a second… How do I join and interact then?

Join another instance! You should start on a more common instance and work your way into the fibers of the Fediverse. Here are some of my reccomendations!

- [`eldritch.cafe`](https://eldritch.cafe) - an instance for queer people

- [`catcatnya.com`](https://catcatnya.com) - an instance for queer people and cats

- [`queer.party`](https://queer.party) - gay instance

- [`kolektiva.social`](https://kolektiva.social) - activism

- and more!

## Alright, I'll download the Mastodon app and get to it!

Please don't. They have similar issues to `mastodon.social`, which is trying to introduce algorithms and centralization into the Fediverse. Use an app like [Tusky](https://tusky.app) or Metatext instead. However, you need to make an account on the website before signing in with an app.

## But why choose another instance if Fedi is the same through all instances?

- **Local users** - people on your instance help curate your content by following and boosting.
- **Different properties** - Different instances can have different themes, emojis, character limits, etc.
- **Different rules** - Different instances can have stricter ([`fedi.aroace.space`](https://fedi.aroace.space), [`transmom.love`](https://transmom.love/about/more)) or more relaxed ([`mstdn.social`](https://mstdn.social/about/more)) rules. The instances with the most relaxed rules tend to get blocked by the more strict rule instances easier.

## All this instance talk makes me wonder how to mention people.

Let's say there are 2 instances, `fedi.aroace.space` and [`toot.site`](https://toot.site). Let's say I am on the former. Let's say I want to mention [Lilith from `fedi.aroace.space`](https://fedi.aroace.space/@lilith). Since me and Lilith are on the same instance, the mentions work like twitter, so I would write `@lilith`.  But, let's say I wanted to mention [Meave from `toot.site`](https://toot.site/@meave). Meave is not on the same instance as me, so I have to find the federating URL (like `toot.site`, [`chir.rs`](https://mastodon.chir.rs)) and put that *after* their normal mention. In this case, it would be `@meave@toot.site`.
### What's the difference between the instance URL and the federating URL?
The federating URL may sometimes be different. Take for example, `chir.rs`: it federates under the name `chir.rs` but the actual location of it is `mastodon.chir.rs`.
## What are these domain blocks you talk about?
It means one instance will reject any federation to or from the said instance. This allows you to efficiently block undesirable people. People call this **defederation** or **fediblocking**. People share recommendations to block instances on the ***#FediBlock*** hashtag, meaning that transphobes and other bad people can be blocked within a day if they do something bad. Now novody on the instances that block it can be affected by their toxicity!
## Sounds dystopic. I don't like it.
Join a free speech instance then! Of course, we won't federate with one another. It's for the best. Access to other instances is a right.
## Why is it so good?
It mostly boils down to two things:
- You do not interact with other instances' content. You interact with a cached copy. Other instances' content is called **remote content**.
- You only see remote content that your instance is aware of.
Instances get remote content through these methods:
- Getting posts from someone a user follows
- Getting a boost (Retweet) from someone a user follows
- A user pasting a URL of a post into the search box
- Checking for the posts/replies a remote reply post is replying to.
### Gib an example!
Let's make up 3 instances. They are aroace.social, bisexual.website, and transgender.services. Let's say @lena@aroace.social made a Public post saying “I like pears”. This post appears in her Home, Local, and Federated timelines. Then she posts “I like grapefruit” with an Unlisted visibility. This only appears on her Home timeline. @maya@aroace.social does not follow Lena, and as such sees the pears post in her Local and Federated timelines but not the Home timeline. She does not see the grapefruit post. However, @manta@bisexual.website follows Lena and bisexual.website gets new posts from aroace.social, getting both the pears and grapefruit posts. She sees the pears posts on the Home and Federated timelines and the grapefruit post on the Home timeline. @bli@transgender.services follows @manta@bisexual.website and Manta boosts the pears post, therefore federating it to transgender.services which did not know about aroace.social before. transgender.services does not know about the grapefruit post at all, until Manta replies to it saying “Same!”. Then, transgender.services gets the replied to and federates it.

Bit of a handful, right? No worries. This isn't too big of a thing. The same thing is for favorites (likes), replies, and boosts (retweets).

## I see people talking about secure mode a lot. What's that?

Take the example from before. Let's say aroace.social defederated from evil.moe. However, bisexual.website federates with both. Let's say @SerialKiller@evil.moe follows Manta. When Manta replies, evil.moe fetches the reply, but then tries to fetch the original post and fails. It then removes its signature as evil.moe and tries to get the post, and success. This is how block evasion happens on Fedi. Secure mode breaks this. It forces all servers to tell it what server they are or it will not give the post. This is a very good feature but **not on by default**. Some instances that have it on are `toot.site` and `catcatnya.com`. This can only be enabled by the admin, so watch out.

## What are these Home and Unlisted things?

They're the names of the timelines. You have 4 main timelines: *Home*, *Notifications*, *Local*, and *Federated*. The *Home* timeline shows the posts of the people you follow in a chronological order. No algorithms. Same with all the timelines. *Notifications* is self-explanatory. *Local* is all of the *Public* posts made by users *on your instance*, and *Federated* is public posts made by *all known users*.

## What's Public post?

Mastodon has 4 visibilities for posts: *Public*, *Unlisted*, *Followers*, and *Direct*. *Public* posts the post to public timelines and hashtags. The post is boostable. *Unlisted* is the same as Public, except it does not appear in public timelines. *Followers* posts the post to only your followers and anyone mentioned in the post, and it is not boostable. *Direct* is the same as *Followers* but it only posts to the people mentioned. These posts are not encrypted, so do not use them to send sensitive info.

## People are complaining at me but I'm doing nothing wrong!

You may be doing these things wrong:
- Not captioning media
- Not using CWs
- Improperly capitalizing hashtags
To caption an image, use the little pencil or A✓ in the corner to add a caption before posting. It is proper etiquette on Fedi to use captions. also use #CamelCaseHashtags like that not #likethis, as the 2nd type is not readable by screen readers.
## What should I CW?
- mh - mental health
- ph - physical health
- selfie ec - selfie with eye contact
- selfie no ec - selfie with no eye contact
- +/- - positive or negative
- irl - real life
- alc - alcohol
- sui - suicide
- lewd, nsfw - what it says on the tin. nafw is a bit spicier though
- meta - posting about the fediverse
- caps - all caps somewhere
- screenreader bad - bad for screenreaders
- tone indicators can be put in the cw

That is a non exhaustive list. if in doubt, cw it.

## There's different software, what are the differences?
### Mastodon
This guide covers using Mastodon and Mastodon apps. All features in this guide are present in Mastodon. I will document *differences* from Mastodon here.
### Hometown
Nearly Mastodon, *except* you can make posts that never leave your instance.
### Glitchsoc
Nearly Hometown, except you can post in Markdown (learn that [here](https://www.markdowntutorial.com/)) and the webapp has more features (like collapsing toots and a pop-in player).
### Akkoma
The webapp for Akkoma is completely different than Mastodon. It also provides extra features such as emoji and custom emoji reacts. However, note that trying to view a post on instances of Akkoma, logged in or not, requires JavaScript unlike Mastodon, Hometown, and Glitchsoc. Akkoma also does not support federating URLs, and uses instance URLs instead.
### Misskey
Misskey is way different than both of these, but has the same features of Akkoma. However, unlike Akkoma, you cannot use it with Mastodon apps. Misskey has very poor accessibility support, and like Akkoma, requires JS (but even more so) to view posts. Misskey does not have secure mode, so I do not reccomend using Misskey.
### GoToSocial
GoToSocial works mostly like Mastodon, except it has secure mode always on and does not have a WebUI. Furthermore, only Public posts are shown on the frontend.

## That's it for now!

Please contact me if you want to add more to this or any questions. Have fun!