---
title: "Observations of People and Gratefulness"
date: 2022-05-03T13:05:55Z
draft: false
---

As I’m looking at these people in front of me having tech problems, I feel grateful for having my own WriteFreely instance, which is now a Hugo blog.

I’m also grateful for having plenty food, and water. Also cats, because I love my cats dearly.

Are you grateful for anything? If so, please tell me, and maybe even the world. Being grateful every day is a good habit that we should have.
