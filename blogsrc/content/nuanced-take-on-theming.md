---
title: "Nuanced Take on Theming"
date: 2022-06-09T09:35:38Z
draft: false
---

This is my nuanced take on theming, that could be considered long. I copied it from a thread I made. This is a slightly less formal post than usual.

## Content

theming is absolutely broken by default and our current system of theming is absolute crap. however, I believe we can fix this and libadwaita is already making great strides toward that. theming is a good idea, but good execution of all parts is very dearly hard to do without breakage

recoloring and adding borders to stuff is something that absolutely will not break and i think that's the first thing to implement

icon themes can create inconsistency as i explain in my problems with customizations blogpost (which is a bit outdated)

custom padding and things may also break on smaller viewports but a couple limited css rules should be good there

I have seen people say „Just use what GTK provides“ but sometimes that's not enough, you need a custom widget: see Roulette rows in my app and the Nautilus directory bar in 42

Theming in GTK is just a broken mess and only the people who have actually worked with GTK seem to understand this

The simpler something is, the easier it is to theme: yet nobody wants simple things anymore

GNOME apps are designed to be as easy as possible to use by the end user and yet people always find something to complain about with them without thinking about the logistics at all

I partially agree with {{< url title="stopthemingmy.app" href="https://stopthemingmy.app/" >}} but do not think there is a brand so to speak

either way, icon sets that are very different from the original cause inconsistencies and I don't think such inconsistencies are what a DE would want to ship when everything else looks one way

I see people get all pissy without reading the whole letter as aforementioned—there are some good points such as

> The problem we’re facing is the expectation that apps can be arbitrarily restyled without manual work, which is and has always been an illusion.

recoloring on its base works better than theming because custom widgets can use the default colors for themselves through variables and when those change those colors also change and it's good

trying to fully restyle an app without any manual work is like html syntax highlighting on go templating html- it just doesn't work for the edge cases which are a lot

> …but assuming every app works with every stylesheet is a bad default.

As stated earlier, edge cases are everywhere, even in the simplest apps. This causes problems with auto restyling- to restyle effectively CSS would have to be individually applied to Every Single App Custom Widget and there are a lot of them- thus recoloring and a couple other tweaks would be much better.

That's how I feel about theming.