---
title: "A Story I’m Writing"
date: 2022-05-06T13:05:33Z
draft: false
---

I’m writing a story, for a school project. It’s about a girl named Jude who has to navigate through racism. It’s not really great, and it’s my first draft. I also haven’t written a story in a while. You can read it below…

## Jude’s Laments

Hi, my name’s Jude. I’m just an ordinary girl. But my life is… slightly different than you would expect. I’ll start at the beginning.

I was born in Syria, to my two loving parents. Life there started to get bad, so we emigrated to America. In Syria, we were told how great America is. We were told you could get anywhere with anything, and everyone is equal. I believed them, until I got here about 5 years ago.

They say it starts with some guy named Zurara, who spread the idea that white people were the superior race. This idea went to the founders of America, who instituted slavery in America of people from Africa. You probably know that last part. But this idea that white people are better extends its hands deep through the history of this country. You have probably heard about the Salem Witch Hunt, right? The witch accusers mostly accused non-White people of being witches. Thomas Jefferson, said to be an American hero and advocate for anti-slavery, actually had slaves. George Washington also had slaves. Racism’s roots extend very, very far and deep into this country.

This simple fact is why I’m writing this story. As I’m Arab, the events of 9/11 are blamed on me even though I was born after it. That brings me to right now. Both my hijab and hair are wet because someone dumped a big bucket of water on me after calling me terrorist. This comes after the nearest Middle Eastern restaurant got a card saying “Go away terrorists”, so it seems the white people aren’t too happy we’re here. They also force us to do work for cheap, just like after Black Americans were liberated from slavery. Things are pretty depressing right now, but there’s a better future.

For a better future, we should respect one another. Wholly, and fully. No “buts”, everyone gets respect. We can put aside our differences; we’re all human, after all. We can spread anti-racist ideas, and fight against this propaganda. The color, gender, orientation, religion, doesn’t matter. We’re all human! If we just recognize that, and love everyone because they’re unique, maybe we can band together to fight greater evils. And some people are already doing this! Malala Yousafzai, Malcolm X, and others too. They have been for a long time, actually. But instead of some people, it should be most people.
