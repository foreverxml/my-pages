---
title: "A New Orleans Thing"
date: 2022-04-30T13:04:37Z
draft: false
---

A Wednesday-Monday blog post.

## Wednesday

### 17:30, Denver time

Hello! It’s me. Sitting in my comfy chair, after updating some help pages, I’m planning on what to pack. I won’t be coming back here tomorrow after school so I’m getting my last computer work finished. Not much here. Until later!

## Thursday

### 07:15

I’m getting my stuff into a bag, and having Thai tea. Nothing much…

### 08:50

I’m in school, first period. Normal, except I’m not carrying a backpack; rather a laptop bag.
<!--more-->

### 11:00

Still in math, with the laptop bag. It’s all just a normal day...

### 13:30

In orchestra, in 30 minutes I’ll be going to the airport. Exciting.

### 14:45

I’m on the way to the airport: the hotel looks pretty.

### 15:05

Ugh, security. I’ve been walking for 3 whole minutes through this thing. And that’s not even all of it...

### 15:45

Eating Chinese food is nice. Too bad my flight’s boarding soon.

### 16:40

Still waiting for the flight, ugh. When will it leave?

### 17:30

Last little bits- the plane’s leaving now! So exciting.

### 17:55

Oh hey. It’s me, again. Thanks for staying. I’m listening to my music while writing this, and hope to read my feeds too. Peace.

### 19:30

Landing time!!!!

## Friday

### 08:20 New Orleans time

Hi. Yesterday we went through getting a rental car and coming to the house. I fell asleep immediately after. So here I am, this morning. Hi.

### 09:30

I just had a granita at PJ’s coffee- it was *very* nice!

### 11:30

After the coffee shop, we walked around. I got a Ukrainian model set from [Ugears](https://ugearsmodels.com), and an old cat meme book. I also took pictures along the way. That’s it for now.

### 17:15

I did a lot. After that, we:

- Went back to the house
- Had lunch with friends
- Walked around Tulane

That takes a sizeable time to do.

## Saturday

### 00:10

While I was gone, I did karate, then went to a piza place. Then, to my friend’s home where I stayed for 2 hours. Then back home for midnight.

### 17:25

I woke up, and went to the coffee shop. Then, I went to karate early. We went home, then to lunch at the St. Patrick’s parade, where we got lots of beads. Then, we went to Hansen’s Snow Bliz and got some stuff, which I’m still eating.

## Sunday

### 12:15, New Orleans Daylight time

I didn’t write about a lot here: sorry! There’s so much happening I have no time to write about it. We went to dinner at Atchafalaya, and then to the same friend’s home. We went to bed, woke up, packed, then went to the coffee shop and we’re going to Oak Alley plantation.

### 18:45

The plantation was nice; we came back to Uptown, and are walking in Audobon Park now.

### 23:00

Plane’s leaving... after a loooong time.

## Monday

### 01:50 Denver Daylight time

You know that comfy chair? I’m back in it. My trip’s over... but jetlag isn’t. Notice the really late time? I have to wake up early for school. Fun times.

### 19:30

I woke up too late to go to school, so I stayed home. I also assembled the Ugears thing. Today was nice.


That’s it for this post, I hope you enjoyed my (very small) compilation of things I did in New Orleans. Peace!
