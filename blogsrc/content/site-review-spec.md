---
title: "My Site Review Specification"
date: 2022-05-27T18:25:21Z
---
I like to review sites that other people made for fun. Here’s what I give feedback on. This will change over time, as this is a living document: be wary.

## Reccomendations

When I review your site, your site will be marked down if it does not follow the reccomendations below.

### Usage of @media prefers-color-scheme

For users that use dark mode, your site should be dark! Otherwise, it should *always be light*. However, do not make your dark theme white on black: that also presents accessibility concerns.

### Constrained Body Width

Your body width should be constrained to 80 characters, 40 for CJK languages. This is to improve readability.

### Font Stack

You should use sans-serif or monospace fonts on your website- or import your own sans-serif or monospace. Other fonts types cause readability issues.

### Semantic HTML

Semantic HTML is an absolute must. Not only does it make it easier for a screenreader to navigate, it also makes it easier for reader-mode programs to parse and just in general improves how a website is. This page uses semantic HTML, and the indent effect (if viewed on the original site) is done with a definition list, giving a "meaning" to all these pairs of headings and descriptions.

### `aria-labelledby`

The `aria-labelledby` property gives elements an accessible name in HTML. Using this will help screenreader users understand a section from its title.

### Tap Targets

Tap targets should be appropriately sized. This means no single-character links; describe them and put them under a details-summary instead.

### No JS-using limitation

I’m shocked that I must put this here, but it’s here nonetheless. If you limit your site to users with JS off, or to a specific browser engine, or anything, that recieves a very bad grade.

### Accessibility, in general

As referenced in my [opinions page,](/opinions) Seirdy’s guide to *Best practices for textual websites* is a very good read. It outlines what you should do to make a website good, and accessible. I could go on listing various things, but that would take a lot of time, wouldn’t it?

Yea, that’s the post. Until next time!