---
title: "Of Isms and Phobias and Other Things"
date: 2022-05-12T17:42:52Z
draft: false
---

### *Sensitive article*

To the person
who shoved
a middle finger
in my face
after calling me
a loser and gay:

your heterosexuality
is so fragile
that you turn
to insulting others,
isn’t it?
Try and
reconsider your
life, please.

To the person
who said
to me
"Just focus,
it’s that easy":

I would do that
but I just
can’t focus on
anything and
I can’t do
anything about
it, so please
stop.

To the person
who I confided in
and then shared
that knowledge:

I wanted to trust you
but you shared
what’s private
and you violated my trust
ever so badly.

To the person
who always asks
for me to be
their girlfriend:

I’ve repeatedly told you
I’m aromantic
and I’m not interested
so please stop.

To the person
who said I’m a girl
and I can’t do anything:

You’d be able
to do things
if you didn’t spend
so much time
putting down others.

To the person
who keeps repeating
that I’m smart
and above the others:

Every
every
every time,
every time you say that

you alienate me more
from everyone else
so please
just stop.

To the person
who always says
that my grades
have to be good:

I try so hard
but my brain goes away
and then everything
falls
down

and apparently
there’s no forgiveness for that
which makes the cycle
longer

in class I try
but it doesn’t all work
so please
leave some forgiveness

To the person
who’s actually there
and wants to help me:

Thank you,
thank you
so very much,
you are amazing.
