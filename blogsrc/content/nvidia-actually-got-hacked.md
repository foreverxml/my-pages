---
title: "NVIDIA Actually Got Hacked"
date: 2022-05-02T13:04:11Z
draft: false
---

*Starting RSS
you’ll see, the freedom of this
new simplicity.*

## **LAPSUS$** asks **NVIDIA** for…

If you’ve been following tech news recently, you’ll know that LAPSUS$, a hacking community from South America, asked for NVIDIA to either:

1. **Go open source**, and release *all* the **drivers** underneath an **open source license**, and maintain them that way, *forever*: or
2. They will release NVIDIA’s inside files and **silicon secrets** for the whole public to see.

NVIDIA had until Friday to choose. And by the title of this article, you know they chose #2.

So, LAPSUS$ has now released *all* of it. In a 20 GB **.rar** file. It’s under a **magnet link**, if you were wondering. To prevent you from legal trouble, I’m not sharing that link. It should be easy to find, though. I’ve already found it.

## But Wait, There’s More

LAPSUS$ has also hacked Samsung. They have released a 190 GB .rar file containing:

- **Bootloaders** for the phones, and their **source code**
- **Biometric** authentication **algorithms**
- On-device **encryption** methods

And more.

## Glossary

So, you read the article. Just a bunch of unintelligible words. Well, all the **bolded words**, like those, will be defined here.

- **LAPSUS$** is a hacking group from South America.
- **NVIDIA** is a popular GPU maker. GPUs are what make you seeing this on a screen possible, and what makes 3D games possible too.
- **Go open source** means to release all of your code under an open license.
- **Drivers** are what make software and hardware interface.
- An **open license** states yhat anyone can copy and modify your code, and depending on the license type, it may have to be released under the same license. [Truth Social](https://truthsocial.com) by Donald Trump is an example of a software that broke an Open Source license, but now they aren’t breaking it.
- NVIDIA’s **silicon secrets** deal with the secrets that go into making their GPUs.
- A **.rar** file is like a .zip file, it compresses many files into 1 for a nice download.
- A **magnet link** refers to a link to a specific torrent. Torrents are files that are shared from computer to computer, called “peer to peer”, versus the standard model of “server to computer”.
- A **bootloader** is what makes your phone work, and basically power on.
- **Source code** is the code used to make something before it’s “built” to work. It can be licensed under an open source license.
- **Biometrics** are fingerprint, iris, and face unlock.
- **Encryption** makes something inaccessible unless you have the “key” for it, like a house or apartment.

That’s all, until next time.
