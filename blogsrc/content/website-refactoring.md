---
title: "Website Refactoring"
date: 2022-05-26T21:30:12Z
draft: true
---

I’ve been refactoring a couple things (everything) on this website. In fact, maybe while I’m at it I’ll take you through the design history of this website.

## The Redesigns

### CSS Wizard: First Design

My first design of this website was on {{< url href="https://foreverxml.codeberg.page" title="foreverxml.codeberg.page" >}} and consisted of a dark mode website with fancy hover and many other things. It was really, really old.

### Paper Hugo: Second Design

Then, I moved to Hugo! I had a complex workflow with a shell script and 3 repos to manage posting to Codeberg Pages my Hugo site. Everything was nice, I had a blog with a theme, I liked it. But then I was eh, a bit bored, and deleted it. It no longer exists, but it was a rudimentary first website.

### The Random Hub: Third Redesign

One weekend this year, 2022, I was a bit bored. So I made a completely new landing website for Random! It looked great, seemed very GNOMEy, and just perfect! I later moved it to a subdirectory and then {{< url href="https://random.amongtech.cc" random.amongtech.cc" >}} where it resides today.

### GNOME Purple: Fourth Redesign

I later adapted that same CSS file to work with any colors, specified in the HTML file itself, and made my website modeled around it. It had little content but lots of images, and I still consider it pretty. It had webring parts, it had everything I could wish for. I even made a blog for it on my WriteFreely! But it was a large website. And couldn’t fit a navigation element. So I deleted that one, too.

### Fifth Redesign: Retro Sepia

That’s the current design. A sepia colored content with a retro scanline effect overlayed. It also has some fun things in it, and is mostly textual. I’ll explain what I added from the original template and why I added it.

## What, How, and Why

The first release of the 5th redesign was not that different; but over the times I added new things to it. I’ll explain all of them and *why* I added them.

### Templated Header and Footer

I added these because with them, I could universally update the header and footer. Later, I updated the template to include the `<head>`.

### Static Blog

I moved from {{< url href="https://writefreely.org" WriteFreely" >}} to {{< url href="https://gohugo.io" title="Hugo" >}} because I wanted more control over how my blog displayed. It’s now also more effecient, so that’s an upside.